Students = LOAD 'datasets/students.tsv' USING PigStorage( '\t', '-noschema') AS (student_id: Long, name: Chararray, surname: Chararray, gender: Chararray, age: Int);

Grades = LOAD 'datasets/grades.tsv' USING PigStorage( '\t', '-noschema') AS (student_id: Long, course: Chararray, mark: Double );


StudentGrades = JOIN Students BY student_id LEFT, Grades BY student_id;
StudentGrades = FOREACH StudentGrades GENERATE Students::student_id AS student_id, course AS course, mark AS mark;

StudentsGr2 = GROUP StudentGrades BY student_id;
StudentsGr2 = FOREACH StudentsGr2 GENERATE group as student_id, StudentGrades as list_marks;

FinalResult = FOREACH StudentsGr2 {
	list_marks = FOREACH list_marks GENERATE course, mark;
	list_marks = ORDER list_marks BY mark DESC;
	list_marks = LIMIT list_marks 3;
	GENERATE student_id, list_marks;

}
