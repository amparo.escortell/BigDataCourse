# Start the namenode daemon
$HADOOP_PREFIX/sbin/hadoop-daemon.sh stop namenode
# Start the datanode daemon
$HADOOP_PREFIX/sbin/hadoop-daemon.sh stop datanode

## Start YARN daemons
# Start the resourcemanager daemon
$HADOOP_PREFIX/sbin/yarn-daemon.sh stop resourcemanager
# Start the nodemanager daemon
$HADOOP_PREFIX/sbin/yarn-daemon.sh stop nodemanager
