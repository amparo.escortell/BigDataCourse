#!/bin/bash
echo "Installing HDFS for Hadoop. Please, carry out this operation ONLY ONE TIME"
$HADOOP_PREFIX/bin/hdfs namenode -format
